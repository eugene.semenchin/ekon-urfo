console.log('');

// Бургер Меню //
$('.burger-menu__btn').on('click', function (e) {
  e.preventDefault;
  $(this).toggleClass('burger-menu-btn_active');
  $('.burger-menu-nav').toggleClass('burger-menu-nav_active');
  $('.header__mobile-menu').toggleClass('header__mobile-menu_active');
});

$('.close-mobile-menu').on('click', function (e) {
  e.preventDefault;
  $('.header__mobile-menu').removeClass('header__mobile-menu_active');
});

// Слайдер //

var swiper = new Swiper('.swiper', {
  loop: true,
  speed: 500,
  autoplay: {
    delay: 2000,
  },
});

// Настройка Aside //

$('.filter-style').styler();
$('.filter-item-drop, filter-extra').on('click', function () {
  $(this).toggleClass('filter-item-drop--active');
  $(this).next().slideToggle('200');
});

$('.js-range-slider').ionRangeSlider({
  type: 'double',
  min: 30000,
  max: 1000000,

});

$('.reset').on('click', function () {
  $('.filter-style').removeClass('checked');
});

$(document).ready(function() {
  toggleFilterClass();

  $('.filter-style').styler();
  $('.mobile-filter-drop').on('click', function () {
    $(this).toggleClass('mobile-filter-drop--active');
    $(this).next().slideToggle('200');
  });
})

function toggleFilterClass() {
  var $tabFilter = $('.tab__filter');

  if ($(window).width() < 1145) {
    $tabFilter.addClass('mobile-filter-drop');
  } else {
    $tabFilter.removeClass('mobile-filter-drop');
  }
};
